import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  constructor( private _router: Router ) { }

  ngOnInit() {
    if( sessionStorage.getItem("data") == null){
      this._router.navigate(['home']);
    }
  }

  public imagePath;
  imgURL: any;
  public message: string;

  number= [];

  image_layer(){
    this.number.push(1);
  }







 
  // preview(files) {
  //   if (files.length == 0)
  //     return;
 
  //   var mimeType = files[0].type;
  //   if (mimeType.match(/image\/*/) == null) {
  //     this.message = "Only images are supported.";
  //     return;
  //   }
 
  //   var reader = new FileReader();
  //   this.imagePath = files;
  //   reader.readAsDataURL(files[0]); 
  //   reader.onload = (_event) => { 
  //     this.imgURL = reader.result; 
  //   }
  // }

}
