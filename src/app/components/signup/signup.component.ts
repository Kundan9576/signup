import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor( private _router: Router ) { }

  ngOnInit() {
  }

  @ViewChild('email', { static: false })
  email_input: any;
  @ViewChild('first_name', { static: false })
  first_input: any;
  @ViewChild('second_name', { static: false })
   second_input: any;
  @ViewChild('age', { static: false })
   age_input: any;
  @ViewChild('birth', { static: false })
  birth_input: any;

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  submit_on( ){
    if(
      this.email_input.nativeElement.value =="" ||
      this.first_input.nativeElement.value =="" ||
      this.second_input.nativeElement.value =="" ||
      this.age_input.nativeElement.value =="" ||
      this.birth_input.nativeElement.value =="" 
    ){
      this.correct = false;
    }
    else{
      this.correct = true;
    }
  }

  chechEmail( checkemail){
  let temp = checkemail.split("@");
  let flagmail = 0;
  if(temp.length==2){
    var temp2=temp[1].split(".");
    if(temp2.length==2){ 
      flagmail=1; 
      this.submit_on();     
    }
    else{
      flagmail=0;
      this.email_input.nativeElement.value = "";
      this.email_input.nativeElement.placeholder = "Please enter correct E-mail";
      this.correct = false;
    }
  }
  else{
   flagmail = 0;
   this.email_input.nativeElement.value = "";
   this.email_input.nativeElement.placeholder = "Please enter correct E-mail";
   this.correct = false;
  }
 
  }


  check_first( value, str ){
    if( this.isNumeric(value) ){
      if(str == "First"){
        this.first_input.nativeElement.value = "";
        this.first_input.nativeElement.placeholder = str + " Name does not contain number";
        this.correct = false;
      }
      else if (str =="Last") {
        this.second_input.nativeElement.value = "";
        this.second_input.nativeElement.placeholder = str + " Name does not contain number";
        this.correct = false;
      }     
    }
    else{
      this.submit_on();
    }

  }
   
  correct:boolean = false;
  dataGet( first_name, second_name, age, birth, email ){
    if( first_name=="" || second_name ==""|| age =="" || birth == "" || email =="" ){
      window.alert("Wrong Input");
    }
    else{
      let data = {
        fistName: first_name,
        LastName: second_name,
        age: age,
        birth: birth,
        email: email
      }
      console.log(data);
      window.alert("data is printed in console");
      sessionStorage.setItem("data", JSON.stringify(data));
      this._router.navigate(['gallery']);
    }
  }

}
