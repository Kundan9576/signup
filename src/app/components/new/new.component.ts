import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  set:boolean = true;
  unset:boolean = false;

  images = [
    {
      url : '',
      title: 'Default Image'
    },
    {
      url : '',
      title: 'Default Image'
    },
    {
      url : '',
      title: 'Default Image'
    },
    {
      url : '',
      title: 'Default Image'
    },
  ];


  delimage(index){
    this.images.splice(index, 1);
  }

  show:number;

  change(i ){
    this.set = false;
    this.unset = true;
    this.show = i;
  }

  imgURL;
  preview(files, title, i) {
    if (files.length == 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      window.alert("Give only image file.");
      return;
    }
 
    var reader = new FileReader();
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
     this.imgURL = reader.result;  
     this.images[i].url = this.imgURL;
     this.images[i].title = title;   
    }
   
  }

  done(i, title){
      this.show = undefined;
      this.unset = false;
      this.set = true;  
      this.images[i].title = title; 
  }

}
